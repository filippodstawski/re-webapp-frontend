import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Nav from './Nav';
import LoginPanel from './LoginPanel';
import { getPublic } from '../utils/call-api';

export default class PublicData extends Component {

  constructor() {
    super()
    this.state = {
    };
  }

  componentDidMount() {
  }

  loginCallback = (token) => {
    this.props.callbackFromLogin(token);
  }

  render() {

    if(!this.props.user.log){

      return (
        <div>
  
          <h3 className="text-center">You must log to get private access</h3>
          <LoginPanel callbackFromLogin={this.loginCallback}/>
  
            <div className="col-sm-12">
              <div className="jumbotron text-center">
                <h2>Get Access By Logging In</h2>
              </div>
            </div>
  
        </div>
      );

    }else{

      return (

        <div>

          <h3 className="text-center"> {this.props.user.email} </h3>

          <div className="col-sm-12">
            <div className="jumbotron text-center">
              <p>You account status: {this.props.user.money}$</p>
              <Link className="btn btn-lg btn-success" to='/transactions'> View Transactions </Link>
            </div>
          </div>

        </div>

      );

    }

  }

}