import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../App.css';
import { logUser, userLogout } from '../utils/auth';

export default class Nav extends Component {

  constructor(props) {
    super(props)
    this.state = {};
  }

  componentDidMount() {
  }

  logout = event => {
    this.props.callbackFromLogout();
  }

  render() {

    if(!this.props.user.log){

      return (
        <nav className="navbar navbar-default">
          <div className="navbar-header">
            <Link className="navbar-brand" to="/">ReptWallet Test</Link>
          </div>
          <ul className="nav navbar-nav">
          </ul>
          <ul className="nav navbar-nav navbar-right">
            <li><Link className="btn btn-info log" to="/">Log In</Link></li>
          </ul>
        </nav>
      );

    }else{

      return (
        <nav className="navbar navbar-default">
          <div className="navbar-header">
            <Link className="navbar-brand" to="/">ReptWallet Test</Link>
          </div>
          <ul className="nav navbar-nav">
          </ul>
          <ul className="nav navbar-nav navbar-right">
            <li><Link to="/"> { this.props.user.email } </Link></li>
            <li><Link className="btn btn-danger log" onClick={this.logout} to="/">Log out</Link></li>
          </ul>
        </nav>
      );

    }

  }
}