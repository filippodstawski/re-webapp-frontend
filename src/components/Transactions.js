import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Nav from './Nav';
import { getTransactionsToUser } from '../utils/call-api';

export default class Transactions extends Component {

  constructor(props) {
    super(props);
    this.state = { transactions: [] };
  }

  getTransactions = () => {
    getTransactionsToUser(this.props.user).then((transactions) => {
      this.setState({ transactions });
    });
  }

  componentDidMount() {
    this.getTransactions();
  }

  render() {

    const { transactions } = this.state;

    return (
      
      <div>
        <h3 className="text-center">Transactions</h3>
        <hr />

        <table className="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">money</th>
              <th scope="col">date</th>
            </tr>
          </thead>
          <tbody>

            {transactions.map((transaction, index) => (
              <tr>
                <th scope="row">{transaction.id}</th>
                <td>{transaction.money}</td>
                <td>{new Date(transaction.date).toLocaleString()}</td>
              </tr>
            ))}

          </tbody>
        </table>

        <div className="col-sm-12">
          <div className="jumbotron text-center">
            <Link className="btn btn-lg btn-success" to='/'>Back to user profile</Link>
          </div>
        </div>
      </div>
    );
  }
}