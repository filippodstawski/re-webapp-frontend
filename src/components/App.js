import React, { Component } from 'react';
import { Switch, Route } from 'react-router';
import Nav from './Nav'
import PublicData from './PublicData'
import Transactions from './Transactions'
import { logUser, parseJwt, getUserFromLocalStorage, userLogout } from '../utils/auth';

export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user : {
                userToken: '',
                token: '',
                id: '',
                email: '',
                money: '',
                log: false               
            }
        };
    }

    componentDidMount(){
        let user = getUserFromLocalStorage();
        this.setState({user: user});
    }

    loginCallback = (token) => {
        logUser(token);
        let user = parseJwt(token);
        user.log = true;
        user.userToken = token;
        this.setState({user: user});
    }

    logoutCallback = () => {
        userLogout();
        let user = getUserFromLocalStorage();
        this.setState({user: user});
    }

    render() {
      return (
        <div>
            <Nav user={this.state.user} callbackFromLogout={this.logoutCallback} />
            <Switch>
                <Route exact path='/' render={ () => <PublicData user={this.state.user} callbackFromLogin={this.loginCallback} />}/>
                <Route path='/transactions' render={ () => <Transactions user={this.state.user} />} />
            </Switch>
        </div>
      );
    }

  }