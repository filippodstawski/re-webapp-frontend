import React from 'react'
import { render } from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import App from './components/App';

render((
  <div className="container">
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </div>
), document.getElementById('root'));