import axios from 'axios';

const BASE_URL = 'http://localhost:9091';

export function getTransactionsToUser(user) {

  const url = `${BASE_URL}/transactions`;
  const authString = `Bearer ${user.token}`;

  let data = new FormData();
  data.append('token', user.userToken);

  return axios.post(url, data, { headers: { Authorization: authString } })
    .then(({ data }) => data)
    .catch((error) => {
      console.log('error ' + error);
    });

}

export function checkUser(email, password) {

  let bodyFormData = new FormData();
  bodyFormData.set('email', email);
  bodyFormData.set('password', password);
  const url = `${BASE_URL}/loginUser`;

  return axios({
    method: 'post',
    url: url,
    data: bodyFormData
  }).then(({ data }) => data)
    .catch(error => {
      console.log(error.response)
    });

}