export function logUser(token) {
    let decodedToken = parseJwt(token);
    localStorage.setItem('userToken', token);
    localStorage.setItem('token', decodedToken["token"]);
    localStorage.setItem('id', decodedToken["id"]);
    localStorage.setItem('email', decodedToken["email"]);
    localStorage.setItem('money', decodedToken["money"]);
}

export function getUserFromLocalStorage(){
    let user = [];
    user.userToken = localStorage.getItem('userToken');
    user.token = localStorage.getItem('token');
    user.id = localStorage.getItem('id');
    user.email = localStorage.getItem('email');
    user.money = localStorage.getItem('money');
    if(user.userToken!==null){
        user.log = true;
    }else{
        user.log = false;
    }
    return user;
}

export function userLogout() {
    localStorage.clear();
}

export function parseJwt(token) {
    let base64Url = token.split('.')[1];
    let base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
}